import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { BookModel } from '../book-model';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
  FormGroupDirective
} from '@angular/forms';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.scss']
})
export class BookFormComponent implements OnInit {
  @Output() bookAdded: EventEmitter<BookModel>;
  @ViewChild(FormGroupDirective) myForm;
  bookFormGroup: FormGroup;
  categories: Array<string> = ['drama', 'comedy', 'sports'];
  title: AbstractControl;
  description: AbstractControl;
  category: AbstractControl;

  constructor(formBuilder: FormBuilder) {
    this.bookAdded = new EventEmitter<BookModel>();
    this.bookFormGroup = formBuilder.group({
      title: ['', [Validators.required, Validators.maxLength(30)]],
      description: ['', Validators.required],
      category: [this.categories[0], Validators.required]
    });
  }

  ngOnInit() {
    this.title = this.bookFormGroup.controls.title;
    this.description = this.bookFormGroup.controls.description;
    this.category = this.bookFormGroup.controls.category;
  }

  onSubmit(form: any): Boolean {
    if (this.bookFormGroup.valid) {
      this.bookAdded.emit(form);
      this.myForm.resetForm();
      this.category.setValue(this.categories[0]);
    }
    return false;
  }
}
