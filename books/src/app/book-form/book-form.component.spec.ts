import {
  MatToolbar,
  MatOptionModule,
  MatSelectModule,
  MatInputModule,
  MatFormFieldModule
} from '@angular/material';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookFormComponent } from './book-form.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EventEmitter } from '@angular/core';
import { BookModel } from '../book-model';

describe('BookFormComponent', () => {
  let component: BookFormComponent;
  let fixture: ComponentFixture<BookFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        FormsModule,
        BrowserModule,
        MatSelectModule,
        MatOptionModule,
        MatInputModule,
        MatFormFieldModule,
        BrowserAnimationsModule
      ],
      declarations: [BookFormComponent, MatToolbar]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should have empty values for book title and description', () => {
    expect(component).toBeTruthy();
  });
  it('should have empty values for title and description', () => {
    expect(component.title.value).toEqual('');
    expect(component.category.value).toEqual(component.categories[0]);
    expect(component.description.value).toEqual('');
  });
  it('form invalid when empty', () => {
    expect(component.bookFormGroup.valid).toBeFalsy();
  });
  it('should not emit an event if the form is not valid', () => {
    const spyEvent: EventEmitter<BookModel> = jasmine.createSpyObj('EventEmitter', ['emit']);
    expect(component.bookFormGroup.valid).toBeFalsy();
    component.bookAdded = spyEvent;
    component.onSubmit({});
    expect(spyEvent.emit).not.toHaveBeenCalled();
  });
  it('should show error message if Title is empty when the user submits the form', () => {
    component.onSubmit({});
    expect(component.bookFormGroup.controls['title'].hasError('required')).toBeTruthy();
  });
  it('should show error message if Description is empty when the user submits the form', () => {
    component.onSubmit({});
    expect(component.bookFormGroup.controls['description'].hasError('required')).toBeTruthy();
  });
  it('should reset the form after successful submit', () => {
    component.onSubmit({
      title: 'Rich Dad Poor Dad',
      category: 'self-help',
      description: 'Should definitely read'
    });
    expect(component.bookFormGroup.controls['title'].value).toEqual('');
    expect(component.bookFormGroup.valid).toBeFalsy();
  });
});
