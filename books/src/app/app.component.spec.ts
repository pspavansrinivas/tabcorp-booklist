import { BookComponentComponent } from './book-component/book-component.component';
import { BookListComponent } from './book-list/book-list.component';
import { BookFormComponent } from './book-form/book-form.component';
import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {
  MatToolbarModule,
  MatInputModule,
  MatFormFieldModule,
  MatSelectModule,
  MatOptionModule,
  MatButtonModule,
  MatCardModule
} from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { BookModel } from './book-model';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatToolbarModule,
        ReactiveFormsModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        MatOptionModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatCardModule
      ],
      declarations: [AppComponent, BookComponentComponent, BookFormComponent, BookListComponent]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have book form and book list components`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const dElement: DebugElement = fixture.debugElement;
    const app = fixture.debugElement.componentInstance;
    expect(dElement.query(By.directive(BookFormComponent))).toBeTruthy();
    expect(dElement.query(By.directive(BookListComponent))).toBeTruthy();
  });

  it('should have empty book list', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app: AppComponent = fixture.debugElement.componentInstance;
    expect(app.books.length).toEqual(0);
  });

  it('should have books added to the list on calling newBookAdded', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app: AppComponent = fixture.debugElement.componentInstance;
    const model: BookModel = new BookModel(
      'Rich Dad Poor Dad',
      'self-help',
      'Book everyone should read'
    );
    app.newBookAdded(model);
    expect(app.books).toEqual(jasmine.objectContaining(model));
  });
});
