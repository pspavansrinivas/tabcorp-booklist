import { Component, OnInit, Input } from '@angular/core';
import { BookModel } from '../book-model';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit {
  @Input() books: Array<BookModel>;
  constructor() {
    this.books = new Array();
  }

  ngOnInit() {}
}
