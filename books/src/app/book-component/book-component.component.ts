import { Component, OnInit, Input } from '@angular/core';
@Component({
  selector: 'app-book-component[title][category][description]',
  templateUrl: './book-component.component.html',
  styleUrls: ['./book-component.component.scss']
})
export class BookComponentComponent implements OnInit {
  @Input() title: string;
  @Input() category: string;
  @Input() description: string;
  constructor() {}

  ngOnInit() {
    if (this.isTitleEmpty()) {
      throw new Error('Title should not be empty');
    }

    if (this.isCategoryEmpty()) {
      throw new Error('Category should not be empty');
    }

    if (this.isDescriptionEmpty()) {
      throw new Error('Description should not be empty');
    }
  }

  isTitleEmpty(): Boolean {
    if (null === this.title || undefined === this.title || '' === this.title.trim()) {
      return true;
    }
    return false;
  }

  isCategoryEmpty(): Boolean {
    if (null === this.category || undefined === this.category || '' === this.category.trim()) {
      return true;
    }
    return false;
  }
  isDescriptionEmpty(): Boolean {
    if (
      null === this.description ||
      undefined === this.description ||
      '' === this.description.trim()
    ) {
      return true;
    }
    return false;
  }
}
