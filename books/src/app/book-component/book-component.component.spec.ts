import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookComponentComponent } from './book-component.component';
import { MatCardTitle, MatCard, MatCardContent } from '@angular/material';
import { Component, ViewChild } from '@angular/core';

describe('BookComponentComponent', () => {
  let testHostFixture: ComponentFixture<TestHostComponent>;

  @Component({
    selector: `app-host-component`,
    template: `
      <app-book-component
        title="Hero"
        category="drama"
        description="something"
      ></app-book-component>
    `
  })
  class TestHostComponent {
    @ViewChild(BookComponentComponent) componentUnderTest: BookComponentComponent;
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BookComponentComponent,
        MatCard,
        MatCardTitle,
        MatCardContent,
        TestHostComponent
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    testHostFixture = TestBed.createComponent(TestHostComponent);
    testHostFixture.detectChanges();
  });

  it('should have same title,category,description as given', () => {
    expect(
      testHostFixture.nativeElement.querySelector('mat-card mat-card-title span').innerText
    ).toEqual('Title: Hero');
    expect(
      testHostFixture.nativeElement.querySelector('mat-card mat-card-title span:nth-child(2)')
        .innerText
    ).toEqual('(drama)');
    expect(
      testHostFixture.nativeElement.querySelector('mat-card mat-card-content span').innerText
    ).toEqual('something');
  });

  it('should not accept empty title or category or description', () => {
    const testHC: TestHostComponent = testHostFixture.componentInstance;
    testHC.componentUnderTest.title = '';
    testHC.componentUnderTest.category = '';
    testHC.componentUnderTest.description = '';
    testHostFixture.detectChanges();
    expect(() => {
      testHC.componentUnderTest.ngOnInit();
    }).toThrow(new Error('Title should not be empty'));

    testHC.componentUnderTest.title = 'some';
    testHC.componentUnderTest.category = '';
    testHostFixture.detectChanges();
    expect(() => {
      testHC.componentUnderTest.ngOnInit();
    }).toThrow(new Error('Category should not be empty'));

    testHC.componentUnderTest.category = 'some';
    testHC.componentUnderTest.description = '';
    testHostFixture.detectChanges();
    expect(() => {
      testHC.componentUnderTest.ngOnInit();
    }).toThrow(new Error('Description should not be empty'));
  });
});
