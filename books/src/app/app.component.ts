import { BookModel } from './book-model';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  /**
   * Instead of doing this, we can have a service to hold the book object
   */
  books: Array<BookModel>;
  constructor() {
    this.books = new Array<BookModel>();
  }
  newBookAdded(newBook: BookModel) {
    this.books.push(newBook);
  }
}
