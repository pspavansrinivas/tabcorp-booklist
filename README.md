**To get started with this project**

Please have the following version of node and npm installed
Node: v10.3.0
NPM: 6.4.1

Install typescript and angular-cli globally,

```
npm i -g typescript @angular/cli@v6-lts
cd books
npm i
```

To run the tests,

```
cd books
ng test
```

To run the app,

```
cd books
ng serve
```

If Docker is installed on the machine, you can run package.sh

```
./package.sh
docker run -p 3000:80 --rm books
http://localhost:3000/ (wait for sometime until nginx loads up)
```

Note - I have included .nvmrc file which has the above mentioned node version.
